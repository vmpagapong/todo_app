package com.vpagapong.todo_app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vpagapong.todo_app.activities.AddEditTodo;
import com.vpagapong.todo_app.adapters.TodoAdapter;
import com.vpagapong.todo_app.interfaces.RecyclerViewClick;
import com.vpagapong.todo_app.models.Todo;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Context context;

    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;
    private ConstraintLayout emptyIndicator;

    private Todo selectedTodo;
    private ArrayList<Todo> todoArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        initializeUI();
    }

    private void initializeUI() {
        emptyIndicator = findViewById(R.id.constraint_EmptyTodo);
        recyclerView = findViewById(R.id.rv_Todo);
        floatingActionButton = findViewById(R.id.fab_Todo);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddEditTodo.class);
                startActivity(intent);
            }
        });
    }

    private void getAllTodo() {
        todoArrayList.clear();
        todoArrayList = Todo.read(context);

        emptyIndicator.setVisibility(todoArrayList.size() == 0 ? View.VISIBLE : View.GONE);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        TodoAdapter todoAdapter = new TodoAdapter(context, todoArrayList);
        todoAdapter.setOnRecyclerviewClick(new RecyclerViewClick() {
            @Override
            public void onRecyclerviewItemClick(View view, int position) {
                selectedTodo = todoArrayList.get(position);

                selectAction();
            }
        });

        recyclerView.setAdapter(todoAdapter);
    }

    private void selectAction() {
        String[] items = {" Edit ", " Delete "};
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Select");
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    Intent intent = new Intent(MainActivity.this, AddEditTodo.class);
                    intent.putExtra("ID", selectedTodo.getId());
                    intent.putExtra("TITLE", selectedTodo.getTitle());
                    intent.putExtra("DESCRIPTION", selectedTodo.getDescription());
                    intent.putExtra("DATE_CREATED", selectedTodo.getDateCreated().getTime());
                    intent.putExtra("DATE_EDITED", selectedTodo.getDateEdited().getTime());
                    startActivity(intent);
                }
                if (which == 1) {
                    openDeleteDialog();
                }
            }
        });

        dialog.create().show();
    }

    private void openDeleteDialog() {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("Delete")
                .setIcon(R.drawable.ic_delete_black)
                .setMessage("Are you sure you want to delete?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteTodo();
                    }
                })
                .setNegativeButton("NO", null)
                .create();
        dialog.show();
    }

    private void deleteTodo() {
        Todo todo = new Todo();
        todo.delete(context, selectedTodo.getId());

        Toast.makeText(context, "Deleted.", Toast.LENGTH_SHORT).show();

        getAllTodo();
    }

    @Override
    protected void onResume() {
        super.onResume();

        getAllTodo();
    }
}

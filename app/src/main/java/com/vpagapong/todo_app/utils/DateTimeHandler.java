package com.vpagapong.todo_app.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeHandler {

    public static String convertDateToDisplayDate(Date date) {
        try {
            DateFormat yearFormat = new SimpleDateFormat("MMM dd yy | hh:mm aa");
            DateFormat dayFormat = new SimpleDateFormat("MMM dd | hh:mm aa");
            DateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
            Calendar calendar = Calendar.getInstance();
            int day = calendar.getTime().getDay();
            int year = calendar.getTime().getYear();

            String stringDate = "";

            Date messageCreated = date;
            if (messageCreated.getYear() == year) {
                if (messageCreated.getDay() == day) {
                    stringDate = timeFormat.format(messageCreated);
                } else {
                    stringDate = dayFormat.format(messageCreated);
                }
            } else {
                stringDate = yearFormat.format(messageCreated);
            }

            return stringDate;
        } catch (Exception err) {
            return null;
        }
    }

    public static String convertDateToStringDate(Date date) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy || hh:mm aa");
            return dateFormat.format(date);
        } catch (Exception err) {
            return null;
        }
    }

    public static Date convertStringToDate(String strDate) {
        try {
            DateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy || hh:mm aa");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }
}

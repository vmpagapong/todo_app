package com.vpagapong.todo_app.utils;

import android.database.Cursor;

import java.util.Date;

public class Converter {

    public static int ObjectToInt(Cursor cursor, String fieldName) {
        try {
            String sValue = cursor.getString(cursor.getColumnIndex(fieldName));
            if (sValue == null || sValue.isEmpty()) return 0;
            return Integer.parseInt(sValue);
        } catch (NumberFormatException err) {
            return 0;
        }
    }

    public static String ObjectToString(Cursor cursor, String fieldName) {
        try {
            return cursor.getString(cursor.getColumnIndex(fieldName));
        } catch (Exception err) {
            return "";
        }
    }

    public static Date ObjectToDate(Cursor cursor, String fieldName) {
        try {
            return DateTimeHandler.convertStringToDate(ObjectToString(cursor, fieldName));
        } catch (Exception err) {
            return null;
        }
    }

    public static boolean ObjectToBoolean(Cursor cursor, String fieldName) {
        try {
            int result = ObjectToInt(cursor, fieldName);
            return result == 1;
        } catch (Exception err) {
            return false;
        }
    }
}

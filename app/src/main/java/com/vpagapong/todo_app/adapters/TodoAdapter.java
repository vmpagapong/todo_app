package com.vpagapong.todo_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.vpagapong.todo_app.R;
import com.vpagapong.todo_app.interfaces.RecyclerViewClick;
import com.vpagapong.todo_app.models.Todo;
import com.vpagapong.todo_app.utils.DateTimeHandler;

import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Todo> todoArrayList;
    private LayoutInflater layoutInflater;
    private RecyclerViewClick recyclerviewClick;

    public TodoAdapter(Context context, ArrayList<Todo> todoArrayList) {
        this.context = context;
        this.todoArrayList = todoArrayList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.listrow_todo, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Todo todo = todoArrayList.get(position);

        holder.tvTitle.setText(todo.getTitle());
        holder.tvDescription.setText(todo.getDescription());
        holder.tvDateCreated.setText(DateTimeHandler.convertDateToDisplayDate(todo.getDateEdited()));
    }

    @Override
    public int getItemCount() {
        return todoArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle, tvDescription, tvDateCreated;
        private ConstraintLayout constraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tv_ListrowTodoTitle);
            tvDescription = itemView.findViewById(R.id.tv_ListrowTodoDescription);
            tvDateCreated = itemView.findViewById(R.id.tv_ListrowTodoDateCreated);
            constraintLayout = itemView.findViewById(R.id.constraint_ListrowTodo);

            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerviewClick != null)
                        recyclerviewClick.onRecyclerviewItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setOnRecyclerviewClick(RecyclerViewClick recyclerviewClick) {
        this.recyclerviewClick = recyclerviewClick;
    }
}

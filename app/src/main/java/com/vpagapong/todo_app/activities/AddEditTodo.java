package com.vpagapong.todo_app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.vpagapong.todo_app.R;
import com.vpagapong.todo_app.models.Todo;
import com.vpagapong.todo_app.utils.DateTimeHandler;

import java.util.Calendar;
import java.util.Date;

public class AddEditTodo extends AppCompatActivity {

    private Context context;

    private EditText etTitle, etDescription;
    private Button btnSave;
    private TextView tvDateCreated, tvDateEdited;

    private int noteId;
    private boolean isUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_todo);

        context = this;

        Intent intent = getIntent();

        initializeUI();

        if (intent.hasExtra("ID")) {
            setTitle("Edit Todo");
            isUpdate = true;
            noteId = intent.getIntExtra("ID", 0);
            etTitle.setText(intent.getStringExtra("TITLE"));
            etDescription.setText(intent.getStringExtra("DESCRIPTION"));
            tvDateEdited.setText(String.format("Date edited: %s", DateTimeHandler.convertDateToDisplayDate(new Date(intent.getLongExtra("DATE_EDITED", 0)))));
            tvDateCreated.setText(String.format("Date created: %s", DateTimeHandler.convertDateToDisplayDate(new Date(intent.getLongExtra("DATE_CREATED", 0)))));
        } else {
            setTitle("Add Todo");
            isUpdate = false;
            tvDateEdited.setVisibility(View.GONE);
            tvDateCreated.setVisibility(View.GONE);
        }
    }

    private void initializeUI() {
        etTitle = findViewById(R.id.et_AddEditTitle);
        etDescription = findViewById(R.id.et_AddEditDescription);
        tvDateCreated = findViewById(R.id.tv_AddEditDateCreated);
        tvDateEdited = findViewById(R.id.tv_AddEditDateEdited);
        btnSave = findViewById(R.id.btn_AddEditSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUpdate)
                    updateTodo();
                else
                    saveTodo();
            }
        });
    }

    private void saveTodo() {
        String title = etTitle.getText().toString();
        String description = etDescription.getText().toString();
        Date dateCreated = Calendar.getInstance().getTime();

        if (title.trim().isEmpty() || description.trim().isEmpty())
            Toast.makeText(context, "Please input title and description.", Toast.LENGTH_SHORT).show();
        else {
            Todo todo = new Todo();
            todo.setTitle(title);
            todo.setDescription(description);
            todo.setDateCreated(dateCreated);
            todo.setDateEdited(dateCreated);
            todo.save(context);

            Toast.makeText(context, "Saved.", Toast.LENGTH_SHORT).show();

            finish();
        }
    }

    private void updateTodo() {
        String title = etTitle.getText().toString();
        String description = etDescription.getText().toString();
        Date dateEdited = Calendar.getInstance().getTime();

        if (title.trim().isEmpty() || description.trim().isEmpty())
            Toast.makeText(context, "Please input title and description.", Toast.LENGTH_SHORT).show();
        else {
            Todo todo = new Todo();
            todo.setTitle(title);
            todo.setDescription(description);
            todo.setDateEdited(dateEdited);
            todo.update(context, noteId);

            Toast.makeText(context, "Saved.", Toast.LENGTH_SHORT).show();

            finish();
        }
    }
}

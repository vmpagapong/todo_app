package com.vpagapong.todo_app.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.widget.Toast;

import com.vpagapong.todo_app.dbClasses.DatabaseAdapter;
import com.vpagapong.todo_app.utils.Converter;
import com.vpagapong.todo_app.utils.DateTimeHandler;

import java.util.ArrayList;
import java.util.Date;

public class Todo {

    private int id;
    private String title;
    private String description;
    private Date dateCreated;
    private Date dateEdited;

    public Todo() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateEdited() {
        return dateEdited;
    }

    public void setDateEdited(Date dateEdited) {
        this.dateEdited = dateEdited;
    }

    public static ArrayList<Todo> read(Context context) {
        ArrayList<Todo> todoArrayList = new ArrayList<>();

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String selectQuery;

            selectQuery = "SELECT * \n" +
                    "FROM tbl_todo \n" +
                    "ORDER BY date_edited \n" +
                    "DESC";

            Cursor cursor = db.read(selectQuery);

            if (cursor.moveToFirst()) {
                do {
                    Todo todo = new Todo();

                    todo.setId(Converter.ObjectToInt(cursor, "id"));
                    todo.setTitle(Converter.ObjectToString(cursor, "title"));
                    todo.setDescription(Converter.ObjectToString(cursor, "description"));
                    todo.setDateCreated(Converter.ObjectToDate(cursor, "date_created"));
                    todo.setDateEdited(Converter.ObjectToDate(cursor, "date_edited"));

                    todoArrayList.add(todo);

                } while (cursor.moveToNext());
            }
            cursor.close();

            DatabaseAdapter.close();

            return todoArrayList;

        } catch (Exception err) {
            Toast.makeText(context, err.toString(), Toast.LENGTH_SHORT).show();
            return todoArrayList;
        }
    }

    public void save(Context context) {
        try {
            DatabaseAdapter db = new DatabaseAdapter(context.getApplicationContext());

            ContentValues contentValues = new ContentValues();

            contentValues.put("title", title);
            contentValues.put("description", description);
            contentValues.put("date_created", DateTimeHandler.convertDateToStringDate(dateCreated));
            contentValues.put("date_edited", DateTimeHandler.convertDateToStringDate(dateEdited));

            db.save(contentValues, "tbl_todo");
        } catch (SQLException err) {
            Toast.makeText(context, err.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void update(Context context, int todoId) {
        try {
            DatabaseAdapter db = new DatabaseAdapter(context);

            ContentValues contentValues = new ContentValues();

            contentValues.put("title", title);
            contentValues.put("description", description);
            contentValues.put("date_edited", DateTimeHandler.convertDateToStringDate(dateEdited));

            db.update(contentValues, "tbl_todo", "id=?", new String[]{String.valueOf(todoId)});
        } catch (SQLException err) {
            Toast.makeText(context, err.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void delete(Context context, int storeId) {
        try {
            DatabaseAdapter db = new DatabaseAdapter(context);

            db.delete("tbl_todo", "id=?", new String[]{String.valueOf(storeId)});
        } catch (SQLException err) {
            Toast.makeText(context, err.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}

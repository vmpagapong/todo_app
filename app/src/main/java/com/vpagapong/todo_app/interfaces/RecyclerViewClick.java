package com.vpagapong.todo_app.interfaces;

import android.view.View;

public interface RecyclerViewClick {
    void onRecyclerviewItemClick(View view, int position);
}

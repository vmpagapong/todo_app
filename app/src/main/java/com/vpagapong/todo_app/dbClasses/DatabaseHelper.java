package com.vpagapong.todo_app.dbClasses;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final String DATABASE_NAME = "db_todo";
    private static final int DATABASE_VERSION = 1;

    private String todoTable;

    public DatabaseHelper(@Nullable Context context, @Nullable SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            initializeTables();

            db.execSQL(todoTable);
        } catch (SQLException err) {

        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void initializeTables() {
        todoTable = "CREATE TABLE tbl_todo(";
        todoTable += "id INTEGER PRIMARY KEY AUTOINCREMENT,";
        todoTable += "title TEXT,";
        todoTable += "description TEXT,";
        todoTable += "date_created DATETIME DEFAULT CURRENT_TIMESTAMP,";
        todoTable += "date_edited DATETIME DEFAULT CURRENT_TIMESTAMP)";
    }
}
